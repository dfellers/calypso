"""
    Copyright (C) 2002-2022 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentFactory import CompFactory
from FaserSCT_GeoModel.FaserSCT_GeoModelConfig import FaserSCT_GeometryCfg


def MCEventsCfg(flags, **kwargs):
    acc = FaserSCT_GeometryCfg(flags)
    mc_events_alg = CompFactory.MCEventsAlg
    acc.addEventAlgo(mc_events_alg (**kwargs))
    return acc

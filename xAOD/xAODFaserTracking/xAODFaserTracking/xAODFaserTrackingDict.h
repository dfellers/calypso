// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODFASERTRACKING_XAODFASERTRACKINGDICT_H
#define XAODFASERTRACKING_XAODFASERTRACKINGDICT_H

// Needed to successfully generate the dictionary in standalone mode:
#if defined(__GCCXML__) and not defined(EIGEN_DONT_VECTORIZE)
#   define EIGEN_DONT_VECTORIZE
#endif // __GCCXML__

// System include(s):
#include <bitset>
 
// EDM include(s):
#include "AthLinks/DataLink.h"
#include "AthLinks/ElementLinkVector.h"
 
// Local include(s):
#include "xAODFaserTracking/TrackContainer.h"
#include "xAODFaserTracking/TrackAuxContainer.h"

#include "xAODFaserTracking/StripClusterContainer.h"
#include "xAODFaserTracking/StripClusterAuxContainer.h"

#include "xAODFaserTracking/StripRawDataContainer.h"
#include "xAODFaserTracking/StripRawDataAuxContainer.h"

#include "xAODFaserTracking/FaserTrackingPrimitives.h"

namespace {
  struct GCCXML_DUMMY_INSTANTIATION_XAODFASERTRACKING {
      
      xAOD::TrackContainer                                              c1;
      DataLink< xAOD::TrackContainer >                                  l1;
      ElementLink< xAOD::TrackContainer >                               l2;
      ElementLinkVector< xAOD::TrackContainer >                         l3;
      std::vector< DataLink< xAOD::TrackContainer > >                   l4;
      std::vector< ElementLink< xAOD::TrackContainer > >                l5;
      std::vector< ElementLinkVector< xAOD::TrackContainer > >          l6;
      std::vector< std::vector< ElementLink< xAOD::TrackContainer > > > l7;

      // Container(s):
      xAOD::StripClusterContainer                                                    c4;
      // Data link(s):
      DataLink< xAOD::StripClusterContainer >                                        pdl1;
      std::vector< DataLink< xAOD::StripClusterContainer > >                         pdl2;
      // Element link(s):
      ElementLink< xAOD::StripClusterContainer >                                     pel1;
      std::vector< ElementLink< xAOD::StripClusterContainer > >                   pel2;
      std::vector< std::vector< ElementLink< xAOD::StripClusterContainer > > >    pel3;

      // Container(s):
      xAOD::StripRawDataContainer                                                    c6;
      // Data link(s):
      DataLink< xAOD::StripRawDataContainer >                                        rdodl1;
      std::vector< DataLink< xAOD::StripRawDataContainer > >                         rdodl2;
      // Element link(s):
      ElementLink< xAOD::StripRawDataContainer >                                        rdoel1;
      std::vector< ElementLink< xAOD::StripRawDataContainer > >                      rdoel2;
      std::vector< std::vector< ElementLink< xAOD::StripRawDataContainer > > >       rdoel3;

  };
}

#endif // XAODTRACKPARTICLE_XAODTRACKPARTICLEDICT_H